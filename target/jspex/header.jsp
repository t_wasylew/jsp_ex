<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>SDA - JSP.</title>
</head>
<body>

    <%! String[] days = {"Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"};%>
<div style="width: 100%">
    <% for (int i = 0; i < days.length; i++) {%>
    <% String style = "";
        if (LocalDate.now().getDayOfWeek().getValue() == (i + 1)) {
            style += "font-weight: bold;";
        }
        if (i == 5) {
            style += "color: gray;";
        }
        if (i == 6) {
            style += "color: red;";
        }
    %>
    <span style=" <%= style%>"><%= days[i] + (i < days.length - 1 ? "&nbsp|&nbsp" : "") %></span>
    <% } %>

</div>

<div style="width: 100%">
    <% Map<String, String> pages = new LinkedHashMap<>();
        pages.put("/index.jsp", "index");
        pages.put("/uptime.jsp", "uptime");
        pages.put("/multiplication.jsp", "multiplication");
        pages.put("/json.jsp", "json");
        pages.put("/simpleForm.jsp", "simple form");
        pages.put("/simpleForm2.jsp", "simple form2");
        pages.put("/kredyt.jsp", "kredyt");
        pages.put("/login.jsp", "login");
        pages.put("/workers.jsp", "workers");
        boolean isFirst = true;
        for (String pageUrl : pages.keySet()) {
            boolean isCurrent = pageUrl.equals(request.getServletPath());
    %>
    <%=isFirst ? "" : "&nbsp|&nbsp"%>
    <a HREF="<%=pageUrl%>" style="<%= isCurrent ? "font-weight: bold;" : "" %>">
    <%=pages.get(pageUrl)%>
    </a>
    <%
            isFirst = false;
        }
    %>
    <%
        String login = "niezalogowany";
        Cookie[] cookies = request.getCookies();
        if (cookies!=null){
            for (Cookie cookie: cookies ) {
                if ("login".equals(cookie.getName())){
                    login = cookie.getValue();
                    break;
                }
            }
        }
    %>
    <div>
        <%= "User: " + login%>
    </div>

</div>