<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>

<br/>
<br/>


<%
    String kwotaInput = request.getParameter("kwota") ;
    String iloscRatInput = request.getParameter("iloscRat") ;
    String oprocentowanieInput = request.getParameter("oprocentowanie") ;
    boolean wyswietlTablele= false;
    int iloscRat=0;
    double kwota=0.0, oprocentowanie=0.0;

    if(kwotaInput!=null && !kwotaInput.trim().isEmpty()
            && iloscRatInput!=null && !iloscRatInput.trim().isEmpty()
            && oprocentowanieInput!=null && !oprocentowanieInput.trim().isEmpty() ){
        wyswietlTablele=true;
        iloscRat = Integer.valueOf(iloscRatInput);
        kwota = Double.valueOf(kwotaInput);
        oprocentowanie = Double.valueOf(oprocentowanieInput)/100.;
    }

%>




<form action="kredyt.jsp" method="post">
    Kwota kredytu: <input type="text" name="kwota" value="<%=kwotaInput!=null ? kwotaInput : ""%>"><br/>
    ilosc rat: <input type="text" name="iloscRat" value="<%=iloscRatInput!=null ? iloscRatInput : ""%>"><br/>
    oprocentowanie: <input type="text" name="oprocentowanie" value="<%=oprocentowanieInput!=null ? oprocentowanieInput : ""%>"><br/>

    <input type="submit" value="Submit">
</form>

<% if (wyswietlTablele){ %>
<table style="border-collapse: collapse;border: 1px solid black;" border="1">
    <tr>
        <th>Rata</th>
        <th>Część odsetek</th>
        <th>Cześć kapitału</th>
        <th>Wartość do spłaty</th>
    </tr>
    <%for(int i = 1; i<=iloscRat; i++){
         double czescKapitalowa = kwota/iloscRat;
         double czescOdsetkowa = (kwota - ((i-1)*czescKapitalowa))*(oprocentowanie/12.);
         double rata = czescKapitalowa+czescOdsetkowa;

        %>
    <tr>
        <td><%=i%></td>
        <td><%=String.format( "%.2f",  czescKapitalowa)%></td>
        <td><%=String.format("%.2f", czescOdsetkowa)%></td>
        <td><%=String.format("%.2f", rata)%></td>
    </tr>

    <%}%>

</table>

<br/>
<br/>
<% } %>






<jsp:include page="footer.jsp"/>
