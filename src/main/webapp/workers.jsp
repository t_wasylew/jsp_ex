<%@ page import="pl.sda.dao.WorkersDAO" %>
<%@ page import="pl.sda.dao.WorkersDAOHibernateImpl" %>
<%@ page import="pl.sda.dto.Worker" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>

<%
    WorkersDAO workersDAO = new WorkersDAOHibernateImpl();
    int counter = 1;%>

    <table style="font-weight: bold" border="solid 2px black">
        <th>
        <td>Imie</td>
        <td>Nazwisko</td>
        </th>
    <%for (Worker worker:workersDAO.getAllWorkes()) {%>
        <tr>
            <td>Worker <%=counter++%>: </td>
            <td><%=worker.getFirstName()%></td>
            <td><%=worker.getLastName()%></td>
            <td><a href="/workers.jsp?action=delete&id=<%=worker.getId()%>">Usun</a></td>
        </tr><%}%>
    </table>

</body>
</html>
