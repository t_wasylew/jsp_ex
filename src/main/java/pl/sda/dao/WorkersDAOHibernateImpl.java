package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.sda.dto.Worker;

import java.util.List;

public class WorkersDAOHibernateImpl implements WorkersDAO {

    private SessionFactory sessionFactory;

    public WorkersDAOHibernateImpl(){
        sessionFactory = new Configuration().configure().buildSessionFactory();

    }

    @Override
    public List<Worker> getWorkesByLastName(String lastname) {
        return null;
    }


    @Override
    public List<Worker> getAllWorkes() {
        Session session = sessionFactory.openSession();
        List<Worker> workers = session.createCriteria(Worker.class).list();
        session.close();
        return workers;
    }


    @Override
    public Worker getWorker(int idWorker) {

        Session session = sessionFactory.openSession();
        Worker worker  = session.get(Worker.class, idWorker);
        session.close();
        return worker;
    }

    @Override
    public void deleteWorker(int workerId) {
        Session session = sessionFactory.openSession();
        Worker worker  = session.get(Worker.class, workerId);
        session.beginTransaction();
        session.delete(worker);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void saveWorker(Worker worker) {
    Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(worker);
        session.flush();
        session.getTransaction().commit();
        session.close();

    }

    @Override
    public void updateWorker(Worker worker) {
        this.saveWorker(worker);
    }
}
